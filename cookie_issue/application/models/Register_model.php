<?php
class Register_model extends CI_Model{
    public function __construt(){
        
        $this->load->database();
    }
    public function insert($data){
        $this->db->insert("users",$data);
		return $this->getUserId($this->db->insert_id());
	
    }
	public function insertDetails($data){
		 $this->db->insert("details",$data);
		return $this->db->insert_id();
	}
	public function getUserId($insert_id){
		$sql="select * from users where user_id=$insert_id";
		 $query=$this->db->query($sql);
        return $query->row_array();
	}
	public function getUserByUserId($user_id){
		
		$sql="select * from details where user_id=$user_id";
		$query=$this->db->query($sql);
        return $query->row_array();
	}
    public function get($username){
        $query = $this->db->get_where("users", array('username' =>$username));
        if($query->row_array())
            return $query->row_array();
        else
           return false;
    }
}
?>