<?php

class Login extends CI_Controller{
	
    public function __construct(){
        parent::__construct();
		 $this->load->helper('url_helper');
		$this->load->library("encryption");
		
    }
	public function index(){
		if(isset($_COOKIE["login_user"])){
			echo "hi";
		}
		else $this->show_login();
		
	}
	public function show_login(){
			$this->load->view("login");
	}
    public function login(){
        $this->load->model('Register_model');
       extract($_POST);
        if($data=$this->Register_model->get($username)){
            
        if(password_verify($pass,$data['pass']))
		{
			$_SESSION['user_id']=$data['user_id'];
			//echo $_SESSION['user_id'];
			$data['data']=$this->Register_model->getUserByUserId($_SESSION['user_id']);
			
			  $signed_in = $this->input->post("signed_in");
			if($signed_in){
                    $cookie_name = "login_user";
                    $user_id_to_login = $_SESSION['user_id'];
                    $encrypt_id = $this->encryption->encrypt($user_id_to_login);
                    $cookie_content = $encrypt_id;
                    $cookie_time = time() + 86400 * 30;
                    $path = "/";
                    setcookie($cookie_name, $cookie_content, $cookie_time, $path);
                } else{
                    $cookie_name = "login_user";
                    $user_id_to_login = $_SESSION['user_id'];
                    $encrypt_id = $this->encryption->encrypt($user_id_to_login);
                    $cookie_content = $encrypt_id;
                    $cookie_time = time() + 3600;
                    $path = "/";
                    setcookie($cookie_name, $cookie_content, $cookie_time, $path);
                }
			
			$this->load->view('sample',$data);
		}
        else
            echo "Invalid credentials!!";
        
        }else
            echo "Invalid credentials!!";
    }
    public function register(){
        $this->load->view("register");
        
    }
	public function logout(){
		//echo $_SESSION['user_id'];
		if(count($_COOKIE) > 0) {
    echo "Cookies are enabled.";
} else {
    echo "Cookies are disabled.";
}
		 $cookie_name = "login_user";
        $user_id_to_logout = $_SESSION['user_id'];
        $encrypt_id = $this->encryption->encrypt($user_id_to_logout);
        $cookie_content = $encrypt_id;
        $cookie_time = time() - 86400 * 30;
        $path = "/";
        setcookie($cookie_name, $cookie_content, $cookie_time, $path);
		
        $_SESSION['user_id'] = "";
        session_destroy();
		$url=base_url();
		header("Location: $url");
	}
    public function registerME(){
        $this->load->model('Register_model');
        if(isset($_POST['registerMe'])){
            extract($_POST);
            $data = array(
            'email'=>$email,
            'username'=>$username,
            'pass'=>password_hash($pass,PASSWORD_BCRYPT)
            );
			$user_data=$this->Register_model->insert($data);
			//$data=$this->Register_model->insert();
		
			$data=array(
			'user_id'=>$user_data['user_id'],
			'name'=>$name,
			'address'=>$address,
			'phone'=>$phone
			);
			$this->Register_model->insertDetails($data);
			header("Location: ../../");
            }
	
        }
    }
    

?>