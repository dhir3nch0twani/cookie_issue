-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2018 at 05:18 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `details_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`details_id`, `user_id`, `name`, `address`, `phone`) VALUES
(1, 8, 'Kusum', 'UNR', '7744844290'),
(3, 10, 'Ramesh', 'Dadar', '7412589630'),
(4, 11, 'Ramesh', 'Dadar', '7412589630'),
(5, 12, 'Chirag Raghani', 'Aurangabad', '9898856040'),
(6, 13, 'Chirag Raghani', 'Aurangabad', '9898856040'),
(7, 14, 'Qwerty', 'Uiop', '123456789');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `username`, `pass`) VALUES
(1, 'dhirenchotwani@gmail.com', 'dc123', 'abc123'),
(2, 'ashokchotwani@gmail.com', 'ac123', 'qwerty'),
(4, 'placeyourself.college@gmail.com', 'random', '$2y$10$L2NGqVY0tyYYcJTFsDFfDOvFOCbegyEoAso8q9J1wtX05TDjXCtKy'),
(5, 'placeyourself.college@gmail.com', 'abc12345', '$2y$10$4xI6Kw3FDXLcDdM4E9SHQOtkR/S/pyCtR4AL11l10CY6Kr/Ci890G'),
(6, 'dhiren123@merry.pink', 'hihi', '$2y$10$cEqJCsVO/Pd9IjbR1PAIeuKHsONJ7EtseOw23yPY5oHQX5o17bShC'),
(7, 'bhatiaramesh98@gmail.com', 'qwerty', '$2y$10$LoSWLxPs7I/cDnOJiqABl.4S9DHft/8/8M/rFEvtjXBk.HrEbqo7G'),
(8, 'kusumchotwani@gmail.com', 'massacare', '$2y$10$u.jVnu8PqAwvUUeL4xgc1uGx/mYcsyGotddXSFwF3pO.JAhkK667u'),
(10, 'bhatiaramesh98@gmail.com', 'qwertyuiop', '$2y$10$xtSpnoGW3DxkxH1H6ExXPuU5XVjO6ROW3i43Pq9fWVqSzzGf1lDzS'),
(11, 'bhatiaramesh98@gmail.com', 'qwertyuiop', '$2y$10$T/J5ZSBNH82OFGyfEhvyXOE5WZqvJT6T93pGe9ijf/vnuOSp8wp9a'),
(12, 'dhiren123@merry.pink', '12345', '$2y$10$TMHfMshTBtXoQCle79CLEuUb6qkoJZK8oIj8w/ZIZtdKCrNrf93Hq'),
(13, 'dhiren123@merry.pink', '12345', '$2y$10$b0hA.pdrZEFuOVKxN8mzWOpMQ4CcBQRQ2nhU.QhbpsWN3lhZGiL.G'),
(14, 'dhiren123@merry.pink', 'qwerty1', '$2y$10$8awEgvIgoIyUChPX1UpaXOV36J2sfNFBYobqe0vGZywMbWu6JUGVC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`details_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
